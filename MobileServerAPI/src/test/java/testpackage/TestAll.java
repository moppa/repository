package testpackage;

import org.junit.runner.JUnitCore;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * The class <code>TestAll</code> builds a suite that can be used to run all
 * of the tests within its package as well as within any subpackages of its
 * package.
 *
 * @author David Garcia Centelles
 * @author Irving David Abundiz Torres
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
	TaskManagerTest.class,
})
public final class TestAll {

    /**
     * Void Constructor.
     */
    private TestAll() {
      
    }
      
	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 18/11/15 21:50
	 */
	public static void main(final String[] args) {
	  
		JUnitCore.runClasses(new Class[] { TestAll.class });
	}
}
