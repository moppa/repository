package persistencepackage;

import persistenceimplementationpackage.DBImplementation;

/**
 * Class that defines different functions to request data 
 * to the database.
 * 
 * @author David Garcia Centelles
 * 
 */
public final class DB {
  
  /**
   * Void Constructor.
   */
  private DB() {
    
  }
  
  /**
   * Creates or initializes the DB. 
   */
  public static void createDB() {
    
    DBImplementation.createDB();
    
  }
  
  /**
   * Returns the number of mobiles registered in the database.
   * @return The number of mobiles registered in the database.
   */
  public static int getNumberOfMobiles() {
    
    return DBImplementation.getNumberOfMobiles();

  }
  
  /**
   * Returns the ID of an available task.
   * @return The ID of an available task or "-1" in case that
   * there's any task available. 
   */
  public static int getAvailableTaskId() {
    
    return DBImplementation.getAvailableTaskId();
    
  }
  
  
  /**
   * Checks the availability of a mobile.
   * @param mobileId The ID of the mobile. 
   * @return 0 in case that the mobile is available, -1 in case that the 
   * mobile doesn't exist and another number in case that the mobile has 
   * another task already assigned. 
   */
  public static int checkMobileAvailability(final int mobileId) {

    return DBImplementation.checkMobileAvailability(mobileId);
        
  }
  
  /**
   * Assigns a task to a mobile phone. 
   * @param mobileId The ID of the mobile.
   * @param taskId The ID of the task.  
   * @return The number to calculate. 
   */
  public static int assignTaskToMobile(
      final int mobileId, final int taskId) {

    return DBImplementation.assignTaskToMobile(mobileId, taskId);
    
  }
  
  /**
   * Assigns a result to the task. 
   * @param mobileId The ID of the mobile.
   * @param taskId The ID of the task.
   * @param result The result sent by the mobile phone.
   * @return The number calculated. 
   */
  public static int assignResultToTask(final String result,
      final int mobileId, final int taskId) {

      return DBImplementation.assignResultToTask(result, mobileId, taskId);
    
  }
  
  /**
   * Creates a Mobile in the database. 
   * @param mobileId Id of the mobile.  
   */
  public static void createMobile(final int mobileId) {
      
    DBImplementation.createMobile(mobileId);
    
  }
  
  /**
   * Deletes a Mobile in the database. 
   * @param mobileId Id of the mobile.  
   */
  public static void deleteMobile(final int mobileId) {
    
    DBImplementation.deleteMobile(mobileId);
    
  }
  
  /**
   * Returns a Task to its original state. 
   * @param taskId Id the task
   */
  public static void resetTask(final int taskId) {

    DBImplementation.resetTask(taskId);
    
   }
}
