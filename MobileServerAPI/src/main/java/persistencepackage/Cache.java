package persistencepackage;

import persistenceimplementationpackage.CacheImplementation;

/**
 * Class that defines different functions to request data 
 * to the cache.
 * 
 * @author David Garcia Centelles
 * 
 */
public final class Cache {
  
  /**
   * Void Constructor.
   */
  private Cache() {
    
  }
  
  /**
   * Finds if a result exists in the cache and adds it if doesn't. 
   * @param r Result.
   * @param n Number.
   */
  public static void addResultIfDoesntExist(final int r, final int n) {
    
    CacheImplementation.addResultIfDoesntExist(r, n);
    
  }
}
