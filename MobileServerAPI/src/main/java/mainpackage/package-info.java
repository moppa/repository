/**
 * This package contains the main classes necessary to configure 
 * and upload the server and define the different functions of the API.
 * <br><br> 
 * Main: Configuration and Upload of Server.
 * <br><br>
 * TaskManager: Define the functions of the API of mobile phones:
 * <ul>
 * <li> getTask: The mobile phone request a task to the server. 
 * <li> sendResult: The mobile phone send the result of a task 
 * to the server. 
 * <li> connectMobile: Register a mobile in the mobile list. 
 * <li> disconnectMobile: Eliminate a mobile in the mobile list. 
 * </ul>
 * 
 * @author David Garcia Centelles
 * 
 */
package mainpackage;