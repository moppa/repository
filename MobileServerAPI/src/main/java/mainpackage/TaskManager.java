package mainpackage;

import java.io.StringReader;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;

//import persistencepackage.Cache;
import persistencepackage.DB;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;

import responsepackage.NotAssigned;
import responsepackage.NotRegistered;
import responsepackage.NotTask;

/**
 * Class that define the functions of the API of mobile phones:
 * <ul> 
 * <li> getTask: The mobile phone request a task to the server. 
 * <li> sendResult: The mobile phone send the result of a task 
 * to the server.
 * <li> connectMobile: Register a mobile in the mobile list. 
 * <li> disconnectMobile: Eliminate a mobile in the mobile list.  
 * </ul> 
 * 
 * @author David Garcia Centelles
 * 
 */
@Path("task")
@Api(value = "/task", description = "Task Manager")
public final class TaskManager {
    
    /**
     * Counter that generates the ID of the Mobile Phones. 
     */
    private static int countmobile = DB.getNumberOfMobiles() + 1;
  
    /**
     * Response 200. 
     */
    static final int R200 = 200;
    
    /**
     * Response 404.
     */
    private static final int R404 = 404;
    
    /**
     * Response 406.
     */
    private static final int R406 = 406;
    
    /**
     * Response 503.
     */
    private static final int R503 = 503;
    
    /**
     * Define the function of the API to get a task from the server.
     * <br><br>
     * Parts:
     * <ul style="list-style-type:none">
     * <li> 1. Finds a task that isn't assigned to a mobile phone. 
     * <li> 1.1. (Negative case) 
     * <li> 1.1.1. Error Code that will be interpreted by the mobile phone 
     * like there're not task available at the moment or that it has already 
     * a task assigned. 
     * <li> 1.2. (Positive case)
     * <li> 1.2.1. Assigns the task to the mobile phone. 
     * <li> 2. Return the response.
     * </ul>
     * @param request String in JSON format that represent the request
     * of the mobile phone.  
     * of the user.
     * @return The response to the user. 
     * (200 OK + Number or 40X/50X "Error"). 
     */
    @POST
    @Path("/getTask")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Get a task from the server")
    @ApiResponses(value = {
        @ApiResponse(code = R200, message = "OK"),
        @ApiResponse(code = R404, message = "Invalid Mobile Phone ID"),
        @ApiResponse(code = R503, message = "Not Available Tasks") })
    public Response getTask(final String request) {
      
      //Interpreting the received string as a JSON. 
      JsonReader jsonReader = Json.createReader(new StringReader(request));
      JsonObject obj = jsonReader.readObject();
      jsonReader.close();
      
      //Extracting the information of the request. 
      int mobileId = obj.getInt("mobile_id");
      
      //Creating the response for the mobile phone. 
      JsonObject response;
      
      //Finding the task ID of a not assigned task. 
      //(If there isn't any, then we got -1)
      int taskId = DB.getAvailableTaskId();

      //Finding if the mobile exists or has any task already assigned.
      //(If the mobile doesn't exists, then we got -1).
      //(If the mobile has any task assigned, then we got something 
      //different of zero). 
      int mobileAvailability = DB.checkMobileAvailability(mobileId);
      
      if (mobileAvailability == -1) {
        
        //If the mobile doesn't exists. 
        throw new NotRegistered("Invalid Mobile Phone ID").except();
        
      } else {
        
        if (taskId == -1 || mobileAvailability != 0) {
          
          throw new NotTask("Not Available Tasks").except();
          
        } else {
          
          //Assigning the task to the mobile phone. 
          int number = DB.assignTaskToMobile(mobileId, taskId);
          
          response = Json.createObjectBuilder()
              .add("number_to_calculate", number)
              .build(); 
        }
      }
      return Response.status(R200).entity(response).build();
      
    }

    /**
     * Define the function of the API to send the result of a task to
     * the server. 
     * <br><br>
     * Parts:
     * <ul style="list-style-type:none"> 
     * <li> 1. Interprets the request as is JSON.
     * <li> 2. checks if the result is invalid. 
     * <li> 3. Checks if the mobile is registered and has a task assigned. 
     * <li> 3.1. (Negative case)
     * <li> 3.1.1. Creates the response that correspond to the error. 
     * <li> 3.2. (Positive case)
     * <li> 3.2.1. Changes the information of the task (State and Result). 
     * <li> 3.2.2. Changes the information of the mobile phone (Assigned task).
     * <li> 3.2.3. Creates the response for notify the mobile phone that the 
     * server is ready to accept request of task. 
     * </ul>
     * @param request String in JSON format that represent the request
     * of the mobile phone.   
     * @return The response to the mobile phone 
     * (200 OK + "READY" or 40X "Error"). 
     */
    @POST
    @Path("/sendResult")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Send the result of a task to the server.")
    @ApiResponses(value = {
        @ApiResponse(code = R200, message = "OK"),
        @ApiResponse(code = R404, message = "Invalid Mobile Phone ID"),
        @ApiResponse(code = R406, message = "Task Not Assigned") })
    public Response sendResult(final String request) {
        
      //Interpreting the received string as a JSON. 
      JsonReader jsonReader = Json.createReader(new StringReader(request));
      JsonObject obj = jsonReader.readObject();
      jsonReader.close();
      
      //Extracting the information of the request. 
      String result = obj.getString("result");
      int mobileId = obj.getInt("mobile_id");
      
      int mobileAvailability = DB.checkMobileAvailability(mobileId);
      
      if (mobileAvailability == -1) {
        
        //If the mobile phone is not in the mobile phones list.  
        throw new NotRegistered("Invalid Mobile Phone ID").except();
        
      } else {
        
        int taskId = mobileAvailability;
        
        if (taskId == 0) {
        
          //If the the mobile phone hasn't a task assigned.
          throw new NotAssigned("Task Not Assigned").except();
          
        } else {

          //If everything okay, we change the state and result of the task,
          //the assigned task of the mobile and create the response for notify
          //mobile that the server is ready to accept request. 
          @SuppressWarnings("unused")
          int number = DB.assignResultToTask(result, mobileId, taskId);
          
          //And we add the result to the cache if it doesn't exists. 
          //Cache.addResultIfDoesntExist(result, number);
          
          return Response.status(R200).build();
          
        }
        
      }
    }    
        
    /**
     * Define the function of the API to send a notification to
     * the server to register a mobile phone. 
     * <br><br>
     * Parts:
     * <ul style="list-style-type:none"> 
     * <li> 1. Create mobile phone. 
     * <li> 2. Create response with the assigned ID of the mobile phone. 
     * </ul>
     * @return The response with the assigned mobile phone ID.
     */
    @GET
    @Path("/connectMobile")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Send a notification to the server to " 
        + "register a mobile phone")
    @ApiResponses(value = {
        @ApiResponse(code = R200, message = "OK") })
    public Response connectMobile() {

        DB.createMobile(countmobile);
        JsonObject response = Json.createObjectBuilder()
           .add("mobile_id", countmobile)
           .build();
        countmobile++;
        
        return Response.status(R200).entity(response).build();
    }   
    
    /**
     * Define the function of the API to send a notification to
     * the server to eliminate a mobile phone. 
     * <br><br>
     * Parts:
     * <ul style="list-style-type:none"> 
     * <li> 1. Find the mobile in the mobile list. 
     * <li> 2. Checks if the mobile has any assigned task. 
     * <li> 2.1. (Positive case)
     * <li> 2.2.1. Changes the state and result of task to initial. 
     * <li> 3. Eliminate the mobile phone from the list. 
     * </ul>
     * @param request String in JSON format that represent the request
     * of the mobile phone.   
     * @return The response.
     */
    @POST
    @Path("/disconnectMobile")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Send a notification to the server to " 
        + "eliminate a mobile phone")
    @ApiResponses(value = {
        @ApiResponse(code = R200, message = "OK"),
        @ApiResponse(code = R404, message = "Invalid Mobile Phone ID") })
    public Response disconnectMobile(final String request) {
      
      //Interpreting the received string as a JSON. 
      JsonReader jsonReader = Json.createReader(new StringReader(request));
      JsonObject obj = jsonReader.readObject();
      jsonReader.close();
      
      //Extracting the information of the request. 
      int mobileId = obj.getInt("mobile_id");
      
      int mobileAvailability = DB.checkMobileAvailability(mobileId);
      
      if (mobileAvailability == -1) {

        throw new NotRegistered("Invalid Mobile Phone ID").except();
        
      } else {
        
        int taskId = mobileAvailability;
        
        if (taskId != 0) {

          //If the mobile phone has a task assigned, changes its state 
          //and result to the initial. 
          DB.resetTask(taskId);
          
        }

        //Eliminate the mobile phone from the list. 
        DB.deleteMobile(mobileId);
        
        return Response.status(R200).build();
        
      }
    }  
}