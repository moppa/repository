package responsepackage;

import javax.ws.rs.core.Response.Status;

/**
 * Custom Response. 
 * The mobile phone hasn't a task assigned. 
 * 
 * @author David Garcia Centelles
 * @author Irving David Abundiz Torres
 */
public class NotAssigned extends AbstractStatusType {

  /**
   * Constructor. 
   * @param reasonPhrase Message to show in the response. 
   */
  public NotAssigned(final String reasonPhrase) {
      super(Status.NOT_ACCEPTABLE, reasonPhrase);
  }
}