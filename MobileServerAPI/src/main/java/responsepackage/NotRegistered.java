package responsepackage;

import javax.ws.rs.core.Response.Status;

/**
 * Custom Response. 
 * The mobile phone is not registered in the server. 
 * 
 * @author David Garcia Centelles
 * @author Irving David Abundiz Torres
 */
public class NotRegistered extends AbstractStatusType {

  /**
   * Constructor. 
   * @param reasonPhrase Message to show in the response. 
   */
  public NotRegistered(final String reasonPhrase) {
      super(Status.NOT_FOUND, reasonPhrase);
  }
}