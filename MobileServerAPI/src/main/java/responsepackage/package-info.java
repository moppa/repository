/**
 * This package contains the auxiliary classes to implement the custom responses
 * for the API. 
 * <br><br> 
 * AbstractStatusType: Necessary to construct the custom responses.
 * <br><br>
 * Responses (Each one implemented in one class): 
 * <ul>
 * <li> NotRegistered: The mobile phone is not registered in the server. 
 * <li> Not Assigned: The mobile phone hasn't a task assigned. 
 * <li> NotTask: There isn't any task available for the mobile phone.   
 * </ul>
 * @author David Garcia Centelles
 * @author Irving David Abundiz Torres
 */
package responsepackage;