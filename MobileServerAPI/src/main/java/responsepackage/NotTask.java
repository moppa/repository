package responsepackage;

import javax.ws.rs.core.Response.Status;

/**
 * Custom Response. 
 * The mobile must wait to request another task. 
 * 
 * @author David Garcia Centelles
 * @author Irving David Abundiz Torres
 */
public class NotTask extends AbstractStatusType {

  /**
   * Constructor. 
   * @param reasonPhrase Message to show in the response. 
   */
  public NotTask(final String reasonPhrase) {
      super(Status.SERVICE_UNAVAILABLE, reasonPhrase);
  }
}