/**
 * This package contains the classes that correspond to the implementation
 * of the functions defined in the interface to the database for the API.
 * <br><br> 
 * DBImplementation: Implement different functions to request data 
 * to the database.
 * 
 * @author David Garcia Centelles
 * 
 */
package persistenceimplementationpackage;