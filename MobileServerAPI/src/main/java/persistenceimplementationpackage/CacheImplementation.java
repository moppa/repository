package persistenceimplementationpackage;

import redis.clients.jedis.Jedis;

/**
 * Class that implement different functions to request data 
 * to the cache.
 * 
 * @author David Garcia Centelles
 * 
 */
public final class CacheImplementation {
  
  /**
   * Void Constructor.
   */
  private CacheImplementation() {
    
  }
  
  /**
   * IP of Redis. 
   */
  private static final String REDIS_IP = "52.34.80.38";
  
  /**
   * Port of Redis.
   */
  private static final int REDIS_PORT = 32773;
 
  /**
   * Finds if a result exists in the cache and adds it if doesn't. 
   * @param r Result.
   * @param n Number. 
   */
  public static void addResultIfDoesntExist(final int r, final int n) {
      Jedis cache = new Jedis(REDIS_IP, REDIS_PORT);
      
      if (!cache.exists("" + n)) {
        cache.set("" + n, "" + r);
      }
      
      cache.close();
  }
  
}
