package persistenceimplementationpackage;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;

/**
 * Class that implement different functions to request data 
 * to the database.
 * 
 * @author David Garcia Centelles
 * 
 */
public final class DBImplementation {
  
  /**
   * Void Constructor.
   */
  private DBImplementation() {
    
  }
  
  /**
   * Keyspace used in the database. 
   */
  
  private static final String KEYSPACE = "moppa";
  
  /**
   * IP of the Database. 
   */
  private static final String DATABASE_IP = "52.34.80.38";
  
  /**
   * Port of the Database. 
   */
  private static final int DATABASE_PORT = 32771;
  
  /**
   * Creates or initializes the DB. 
   */
  public static void createDB() {
    
      String table1 = "CREATE TABLE tasks (taskId int,numberToCalculate int,"
          + "userId int,result varchar,state int,PRIMARY KEY (taskId,userId));";
      String table2 = "CREATE TABLE mobiles (mobileId int,assignedTaskId int,"
          + "PRIMARY KEY (mobileId));";
      String table3 = "CREATE TABLE users (userId int,PRIMARY KEY (userId));";
      
      Cluster cluster = getCluster();
      
      if (cluster.getMetadata().getKeyspace(KEYSPACE) == null) {
        
        Session session = cluster.connect();
        String query = "CREATE KEYSPACE " + KEYSPACE + " WITH replication "
            + "= {'class':'SimpleStrategy', 'replication_factor':1};";
        session.execute(query);
        
        session.execute("USE " + KEYSPACE);
        
        session.execute(table1);
        session.execute(table2);
        session.execute(table3); 
        
      } 
      
      cluster.close();
  }
  
  /**
   * Returns the number of mobiles registered in the database.
   * @return The number of mobiles registered in the database.
   */
  public static int getNumberOfMobiles() {
    
    Cluster cluster = getCluster();
    Session session = cluster.connect(KEYSPACE);
    ResultSet r = session.execute("SELECT * FROM mobiles;");
    int count = 0;
    for (@SuppressWarnings("unused") Row row : r) {
      count++;
    }
    cluster.close();
    return count;
  }
  
  /**
   * Returns the ID of an available task.
   * @return The ID of an available task or "-1" in case that
   * there's any task available. 
   */
  public static int getAvailableTaskId() {
    
    Cluster cluster = getCluster();
    Session session = cluster.connect(KEYSPACE);
    ResultSet results = session.execute("SELECT * FROM tasks;");
    int id = -1;
    
    for (Row r : results) {
      if (r.getInt("state") == 0 && id == -1) {
        id = r.getInt("taskid");
      }
    }
     
    cluster.close();
    return id;
  }
  
  /**
   * Checks the availability of a mobile.
   * @param mobileId The ID of the mobile. 
   * @return 0 in case that the mobile is available, -1 in case that the 
   * mobile doesn't exist and another number in case that the mobile has 
   * another task already assigned. 
   */
  public static int checkMobileAvailability(final int mobileId) {
    
    Cluster cluster = getCluster();
    Session session = cluster.connect(KEYSPACE);
    ResultSet results = session.
        execute("SELECT * FROM mobiles WHERE mobileid=" + mobileId);
    int availability = -1;
    for (Row r : results) {
       availability = r.getInt("assignedtaskid");
    }
    cluster.close();
    return availability;
  }
  
  /**
   * Assigns a task to a mobile phone. 
   * @param mobileId The ID of the mobile.
   * @param taskId The ID of the task.  
   * @return The number to calculate. 
   */
  public static int assignTaskToMobile(
      final int mobileId, final int taskId) {
    
    Cluster cluster = getCluster();
    Session session = cluster.connect(KEYSPACE);
    int number = 0, userId = 0;
    
    ResultSet results = session.
        execute("SELECT * FROM tasks WHERE taskid=" + taskId);
    
    for (Row r : results) {
      number = r.getInt("numbertocalculate");
      userId = r.getInt("userid");
    }
    
    session.execute("UPDATE tasks SET state=1 WHERE taskid="
        + taskId + " and userid=" + userId);
    session.execute("UPDATE mobiles SET assignedtaskid="
        + taskId + " WHERE mobileid=" + mobileId);
    
    cluster.close();
    return number;
  }
  
  /**
   * Assigns a result to the task. 
   * @param mobileId The ID of the mobile.
   * @param taskId The ID of the task.
   * @param result The result sent by the mobile phone.
   * @return The number calculated. 
   */
  public static int assignResultToTask(final String result,
      final int mobileId, final int taskId) {
    
    Cluster cluster = getCluster();
    Session session = cluster.connect(KEYSPACE);
    int userId = 0;
    int number = 0;
    
    ResultSet results = session.
        execute("SELECT * FROM tasks WHERE taskid=" + taskId);
    
    for (Row r : results) {
      userId = r.getInt("userid");
      number = r.getInt("numbertocalculate");
    }
    
    session.execute("UPDATE tasks SET state=2,result='" 
        + result + "' WHERE taskid=" + taskId + " and userid=" + userId);
    session.execute("UPDATE mobiles SET assignedtaskid=0"
        + " WHERE mobileid=" + mobileId);
    
    cluster.close();
    return number;
  }
  
  /**
   * Creates a Mobile in the database. 
   * @param mobileId Id of the mobile.  
   */
  public static void createMobile(final int mobileId) {
      
      Cluster cluster = getCluster();
      Session session = cluster.connect(KEYSPACE);
      session.execute("INSERT INTO mobiles "
            + "(mobileid, assignedtaskid) "
            + "VALUES (" + mobileId + ",0)");
      cluster.close();
  }
  
  /**
   * Deletes a Mobile in the database. 
   * @param mobileId Id of the mobile.  
   */
  public static void deleteMobile(final int mobileId) {
      
      Cluster cluster = getCluster();
      Session session = cluster.connect(KEYSPACE);
      session.execute("DELETE FROM mobiles WHERE mobileid="
            +  mobileId);
      cluster.close();
  }
  
  /**
   * Returns a Task to its original state. 
   * @param taskId Id the task
   */
  public static void resetTask(final int taskId) {
    
    Cluster cluster = getCluster();
    Session session = cluster.connect(KEYSPACE);
    int userId = 0;
    
    ResultSet results = session.
        execute("SELECT * FROM tasks WHERE taskid=" + taskId);
    
    for (Row r : results) {
      userId = r.getInt("userid");
    }
    
    session.execute("UPDATE tasks SET result='',state=0 WHERE taskid="
        + taskId + " and userid=" + userId);
    
    cluster.close();
   }
  
    /**
     * Gets the Cluster. 
     * @return The cluster. 
     */
    private static Cluster getCluster() {
       return Cluster.builder().addContactPoint(DATABASE_IP)
          .withPort(DATABASE_PORT).build();
    }   
}
