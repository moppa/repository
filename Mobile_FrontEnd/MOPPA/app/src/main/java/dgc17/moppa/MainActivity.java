package dgc17.moppa;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;

import java.math.BigInteger;

public class MainActivity extends AppCompatActivity {

    private int id;
    private AsyncHttpClient client = new AsyncHttpClient();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final TextView id_text = (TextView) findViewById(R.id.id_value);
        final TextView state_text = (TextView) findViewById(R.id.state_value);
        final Button button_end = (Button) findViewById(R.id.button_end);
        final Button button_request = (Button) findViewById(R.id.button);
        final TextView last_n = (TextView) findViewById(R.id.last_n_n);
        final TextView last_r = (TextView) findViewById(R.id.last_r_n);

        client.get("http://52.33.38.113:8080/moppa/task/connectMobile", new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                String json_object = new String(responseBody);

                try {
                    JSONObject obj = new JSONObject(json_object);
                    id = obj.getInt("mobile_id");
                    id_text.setText(obj.get("mobile_id").toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

                id_text.setText(R.string.not_found);
                state_text.setText(R.string.try_later);
            }
        });

        button_end.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                finish();
            }
        });

        button_request.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                state_text.setText(R.string.working);
                button_request.setEnabled(false);

                try {
                    JSONObject obj = new JSONObject();
                    obj.put("mobile_id", id);
                    StringEntity str = new StringEntity(obj.toString());
                    client.post(getApplicationContext(),
                            "http://52.33.38.113:8080/moppa/task/getTask",
                            str, "application/json", new AsyncHttpResponseHandler() {
                                @Override
                                public void onSuccess(int statusCode, Header[] headers,
                                                      byte[] responseBody) {

                                    String json_object = new String(responseBody);
                                    int number_to_calculate;
                                    String result;
                                    BigInteger r;

                                    try {
                                        JSONObject obj = new JSONObject(json_object);
                                        number_to_calculate = obj.getInt("number_to_calculate");

                                        last_n.setText("" + number_to_calculate);
                                        r = factorial(number_to_calculate);
                                        last_r.setText(r.toString());

                                        result = r.toString();

                                        JSONObject obj_result = new JSONObject();
                                        obj_result.put("result", result);
                                        obj_result.put("mobile_id", id);
                                        StringEntity str_result = new StringEntity(obj_result.toString());

                                        client.post(getApplicationContext(),
                                                "http://52.33.38.113:8080/moppa/task/sendResult",
                                                str_result, "application/json", new AsyncHttpResponseHandler() {
                                                    @Override
                                                    public void onSuccess(int statusCode, Header[] headers,
                                                                          byte[] responseBody) {}
                                                    @Override
                                                    public void onFailure(int statusCode, Header[] headers,
                                                                          byte[] responseBody, Throwable error) {}
                                                });
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    } catch (UnsupportedEncodingException e) {
                                        e.printStackTrace();
                                    }

                                    state_text.setText(R.string.state_value);
                                    button_request.setEnabled(true);
                                }
                                @Override
                                public void onFailure(int statusCode, Header[] headers,
                                                      byte[] responseBody, Throwable error) {
                                    state_text.setText(R.string.no_task);
                                    button_request.setEnabled(true);
                                }
                            });
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

            }
        });
    }

    public void onDestroy() {
        try {
            JSONObject obj = new JSONObject();
            obj.put("mobile_id", id);
            StringEntity str = new StringEntity(obj.toString());
            client.post(getApplicationContext(),
                    "http://52.33.38.113:8080/moppa/task/disconnectMobile",
                    str, "application/json", new AsyncHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers,
                                              byte[] responseBody) {}
                        @Override
                        public void onFailure(int statusCode, Header[] headers,
                                              byte[] responseBody, Throwable error) {}
                    });
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        super.onDestroy();
    }

    private BigInteger factorial (int n) {
        BigInteger result;
        if (n == 1) {
            result = BigInteger.ONE;
        } else {
            result = factorial(n-1).multiply(BigInteger.valueOf(n));
        }
        return result;
    }
}
