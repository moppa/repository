describe('MOPPA Final User Front End Tests', function() {

    var $rootScope, $scope, $http, $interval, $controller;

    beforeEach(module('MOPPA'));

    beforeEach(inject(function(_$rootScope_, _$controller_){       
        $rootScope = _$rootScope_;
        $scope = $rootScope.$new();
        $controller = _$controller_;

        $controller('MOPPA_Controller', {'$rootScope' : $rootScope, '$scope': $scope});
   	}));

    it('Test 0: Should pass.', function() {
        expect(true).toBe(true);
    });
	
	describe('Test 1: Generate User Id', function() {
	
    it("Test 1-1: 200 OK", inject(function($httpBackend,$http) {
		$scope.getUserId();
		$httpBackend
        	.when('GET', 'http://52.34.76.249:8080/moppa/task/getUserId')
        	.respond(200);
		$httpBackend.flush();
        expect($scope.statusId).toBe("Your User ID is: ");
    }));
	
	it("Test 1-2: 400 Error", inject(function($httpBackend,$http) {
		$scope.getUserId();
		$httpBackend
        	.when('GET', 'http://52.34.76.249:8080/moppa/task/getUserId')
        	.respond(400);
		$httpBackend.flush();
        expect($scope.statusId).toBe("Error retrieving Id, please refresh the page or try it later.");
    }));
		
	});
	
	describe('Test 2: Send Task', function() {
	
    it("Test 2-1: 200 OK", inject(function($httpBackend,$http) {
		$scope.sendTask();
		$httpBackend
        	.when('POST', 'http://52.34.76.249:8080/moppa/task/sendTask')
        	.respond(200);
		$httpBackend.flush();
        expect($scope.error).toBe("(Ok)");
    }));
	
	it("Test 2-2: 400 Error", inject(function($httpBackend,$http) {
		$scope.sendTask();
		$httpBackend
        	.when('POST', 'http://52.34.76.249:8080/moppa/task/sendTask')
        	.respond(400);
		$httpBackend.flush();
        expect($scope.error).toBe("(Error: The number must between 1 and 100)");
    }));
		
	});
	
	describe('Test 3: See Tasks', function() {
	
    it("Test 3-1: 200 OK", inject(function($httpBackend,$http) {
		$scope.seeTasks();
		$httpBackend
        	.when('POST', 'http://52.34.76.249:8080/moppa/task/seeTasks')
        	.respond(200);
		$httpBackend.flush();
        expect($scope.results).not.toBe("");
    }));
	
	it("Test 3-2: 400 Error", inject(function($httpBackend,$http) {
		$scope.seeTasks();
		$httpBackend
        	.when('POST', 'http://52.34.76.249:8080/moppa/task/seeTasks')
        	.respond(400);
		$httpBackend.flush();
        expect($scope.results).toBe("");
    }));
		
	});
	
	describe('Test 4: Erase History', function() {
	
    it("Test 4-1: 200 OK", inject(function($httpBackend,$http) {
		$scope.eraseHistory();
		$httpBackend
        	.when('POST', 'http://52.34.76.249:8080/moppa/task/eraseHistory')
        	.respond(200);
		$httpBackend.flush();
        expect($scope.results).toBe("");
    }));
	
	it("Test 4-2: 400 Error", inject(function($httpBackend,$http) {
		$scope.eraseHistory();
		$httpBackend
        	.when('POST', 'http://52.34.76.249:8080/moppa/task/eraseHistory')
        	.respond(400);
		$httpBackend.flush();
        expect($scope.results).toBe("");
    }));
		
	});
});