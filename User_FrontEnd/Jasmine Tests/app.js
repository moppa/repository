angular.module('MOPPA', [])
    .controller('MOPPA_Controller', function ($scope, $http, $interval, $window){
    	
	$scope.getUserId = function() {
        $scope.statusId = "Retrieving User ID. Please wait...";
        $http({
            method: 'GET',
            url: 'http://52.34.76.249:8080/moppa/task/getUserId'
        }).
        success(function(data, status, headers, config) {
        	$scope.statusId = "Your User ID is: ";
			//$scope.userId = data.user_id;

            }).
        error(function(data, status, headers, config) {
        $scope.statusId = "Error retrieving Id, please refresh the page or try it later.";
        });
    }

   	$scope.sendTask = function() {
		$scope.error = "(Sending job...)";
		$scope.last_number = $scope.number;
		$scope.number = "";
     		$http({
                method: 'POST',
                url: 'http://52.34.76.249:8080/moppa/task/sendTask',
			data: { 'user_id' : $scope.userId , 'number_to_calculate' : $scope.last_number }
            }).
            success(function(data, status, headers, config) {
			$scope.error = "(Ok)";
			            }).
            error(function(data, status, headers, config){	
			$scope.error = "(Error: The number must between 1 and 100)";				
           });
   	};

   	$scope.seeTasks = function() {
			$scope.results = "";
     		$http({
                method: 'POST',
                url: 'http://52.34.76.249:8080/moppa/task/seeTasks',
			data: { 'user_id' : $scope.userId }
            }).
            success(function(data, status, headers, config) {
			$scope.results = data;		 
		 });
   	};


   	$scope.eraseHistory = function() {
     		$http({
                method: 'POST',
                url: 'http://52.34.76.249:8080/moppa/task/eraseHistory',
			data: { 'user_id' : $scope.userId }
            });
		 $scope.results = "";
   	};

$window.onbeforeunload = $scope.eraseHistory;

$interval(function(){
     		$http({
                method: 'POST',
                url: 'http://52.34.76.249:8080/moppa/task/seeTasks',
			data: { 'user_id' : $scope.userId }
            }).
            success(function(data, status, headers, config) {
			$scope.results = data;		 
		 });
},5000);

});
