/**
 * This package contains the classes that correspond to the interface
 * to the database for the API.
 * <br><br> 
 * DB: Defines different functions to request data to the database.
 * 
 * @author David Garcia Centelles
 *
 */
package persistencepackage;