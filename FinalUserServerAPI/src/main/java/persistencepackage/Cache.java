package persistencepackage;

import persistenceimplementationpackage.CacheImplementation;

/**
 * Class that defines different functions to request data 
 * to the cache.
 * 
 * @author David Garcia Centelles
 * 
 */
public final class Cache {
  
  /**
   * Void Constructor.
   */
  private Cache() {
    
  }
  
  /**
   * Finds if a result exists in the cache. 
   * @param n Number.
   * @return The result or 0. 
   */
  public static int findResultInCache(final int n) {
    
    return CacheImplementation.findResultInCache(n);
  }
}
