package persistencepackage;

import javax.json.JsonArray;

import persistenceimplementationpackage.DBImplementation;


/**
 * Class that defines different functions to request data 
 * to the database.
 * 
 * @author David Garcia Centelles
 * 
 */
public final class DB {
  
  /**
   * Void Constructor.
   */
  private DB() {
    
  }
  
  /**
   * Creates or initializes the DB. 
   */
  public static void createDB() {
    
    DBImplementation.createDB();
    
  }
  
  /**
   * Creates a Task in the database. 
   * @param taskid Id of the task. 
   * @param userid Id of the user. 
   * @param number Number to calculate. 
   * @param result The result of the task. 
   */
  public static void createTask(final int taskid, final int userid, 
      final int number, final String result) {
      
      DBImplementation.createTask(taskid, userid, number, result);
      
  }
  
  /**
   * Get the list of tasks of the user.  
   * @param userid Id of the user. 
   * @return An JsonArray with the list of tasks of the user. 
   */
  public static JsonArray getTasksOfUser(final int userid) {  
    
    return DBImplementation.getTasksOfUser(userid);

  }
  
  /**
   * Returns the number of tasks stored in the database.
   * @return The number of tasks stored in the database.
   */
  public static int getNumberOfTasks() {
    
    return DBImplementation.getNumberOfTasks();

  }
  
  /**
   * Returns the number of users stored in the database.
   * @return The number of users stored in the database.
   */
  public static int getNumberOfUsers() {
    
    return DBImplementation.getNumberOfUsers();

  }
  
  /**
   * Checks if an user has a task registered in the server. 
   * @param userId The ID of the user. 
   * @return True/False. 
   */
  public static boolean isUserRegistered(final int userId) {

    return DBImplementation.isUserRegistered(userId);
    
  }
  
  /**
   * Erases all the tasks of an specific user. 
   * @param userId The ID of the user.  
   */
  public static void eraseTasksfromUser(final int userId) {
    
    DBImplementation.eraseTasksfromUser(userId);
    
  }
  
  /**
   * Creates an user in the database. 
   * @param userId The ID of the user.  
   */
  public static void createUser(final int userId) {
    
    DBImplementation.createUser(userId);
    
  }
}
