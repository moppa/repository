package persistenceimplementationpackage;

import redis.clients.jedis.Jedis;

/**
 * Class that implement different functions to request data 
 * to the cache.
 * 
 * @author David Garcia Centelles
 */
public final class CacheImplementation {
  
  /**
   * Void Constructor.
   */
  private CacheImplementation() {
    
  }
  
  /**
   * IP of Redis. 
   */
  private static final String REDIS_IP = "52.34.80.38";
  
  /**
   * Port of Redis.
   */
  private static final int REDIS_PORT = 32773;

  /**
   * Finds if a result exists in the cache. 
   * @param n Number.
   * @return The result or 0. 
   */
  public static int findResultInCache(final int n) {
    int result;
    Jedis cache = new Jedis(REDIS_IP, REDIS_PORT);
    if (cache.exists("" + n)) {
      String r = cache.get("" + n);
      result = Integer.getInteger(r);
    } else {
      result = 0;
    }
    cache.close();

    return result;
  }
  
}
