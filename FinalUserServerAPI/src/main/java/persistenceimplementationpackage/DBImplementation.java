package persistenceimplementationpackage;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;

/**
 * Class that implement different functions to request data 
 * to the database.
 * 
 * @author David Garcia Centelles
 * 
 */
public final class DBImplementation {
  
  /**
   * Void Constructor.
   */
  private DBImplementation() {
    
  }
  
  /**
   * IP of the Database. 
   */
  private static final String DATABASE_IP = "52.34.80.38";
  
  /**
   * Port of the Database. 
   */
  private static final int DATABASE_PORT = 32771;
  
  /**
   * Keyspace used in the database. 
   */
  private static final String KEYSPACE = "moppa";
  
  /**
   * Creates or initializes the DB. 
   */
  public static void createDB() {
    
      String table1 = "CREATE TABLE tasks (taskId int,numberToCalculate int,"
          + "userId int,result varchar,state int,PRIMARY KEY (taskId,userId));";
      String table2 = "CREATE TABLE mobiles (mobileId int,assignedTaskId int,"
          + "PRIMARY KEY (mobileId));";
      String table3 = "CREATE TABLE users (userId int,PRIMARY KEY (userId));";
      
      Cluster cluster = getCluster();

      if (cluster.getMetadata().getKeyspace(KEYSPACE) == null) {
        
        Session session = cluster.connect();
        String query = "CREATE KEYSPACE " + KEYSPACE + " WITH replication "
            + "= {'class':'SimpleStrategy', 'replication_factor':1};";
        session.execute(query);
        
        session.execute("USE " + KEYSPACE);
        
        session.execute(table1);
        session.execute(table2);
        session.execute(table3); 
        
      } 
      
      cluster.close();
  }
  
  /**
   * Creates a Task in the database. 
   * @param taskid Id of the task. 
   * @param userid Id of the user. 
   * @param number Number to calculate.
   * @param result The result of the task.  
   */
  public static void createTask(final int taskid, final int userid, 
      final int number, final String result) {
    
      Cluster cluster = getCluster();
      Session session = cluster.connect(KEYSPACE);
      session.execute("INSERT INTO tasks "
            + "(taskid, numbertocalculate, userid, result, state) "
            + "VALUES (" + taskid + "," + number + "," 
            + userid + ",'" + result + "',0);");
      cluster.close();
  }
  
  /**
   * Get the list of tasks of the user.  
   * @param userid Id of the user. 
   * @return An JsonArray with the list of tasks of the user. 
   */
  public static JsonArray getTasksOfUser(final int userid) {  
    
    Cluster cluster = getCluster();
    Session session = cluster.connect(KEYSPACE);
    ResultSet results = session.
        execute("SELECT * FROM tasks WHERE userid=" 
            + userid + " ALLOW FILTERING");
    
    //Builder necessary to create the JSON Array. 
    JsonArrayBuilder builder = Json.createArrayBuilder();
    String s = "";
    int state, number;
    String result;
    
    for (Row r : results) {
      
      state = r.getInt("state");
      number = r.getInt("numbertocalculate");
      result = r.getString("result");
      
      //Changing the state of the state for a more something 
      //more understandable. 
      
      if (state == 0) {
        s = "PROCESSING REQUEST";
      }
      if (state == 1) {
        s = "PROCESSING TASK";
      }
      if (state == 2) {
        s = "TASK FINISHED";
      }

      //Adding the JSON Object that corresponds to the task
      //to the JSON Array. 
      builder.add(Json.createObjectBuilder()
          .add("Number", number)
          .add("Status", s)
          .add("Result", result));
    }
    
    cluster.close();
    return builder.build();
    
  }
  
  /**
   * Returns the number of tasks stored in the database.
   * @return The number of tasks stored in the database.
   */
  public static int getNumberOfTasks() {
   
    Cluster cluster = getCluster();
    Session session = cluster.connect(KEYSPACE);
    ResultSet r = session.execute("SELECT * FROM tasks;");
    int count = 0;
    for (@SuppressWarnings("unused") Row row : r) {
      count++;
    }
    cluster.close();
    return count;
  }
  
  /**
   * Erases all the tasks of an specific user. 
   * @param userId The ID of the user.  
   */
  public static void eraseTasksfromUser(final int userId) {
    
    Cluster cluster = getCluster();
    Session session = cluster.connect(KEYSPACE);
    ResultSet r = session.execute("SELECT * FROM tasks WHERE userid=" 
           + userId + " ALLOW FILTERING");
    for (Row row : r) {
      session.execute("DELETE FROM tasks WHERE taskid=" 
          + row.getInt("taskid"));
    }
    cluster.close();
  }
  
  /**
   * Creates an user in the database. 
   * @param userId The ID of the user.  
   */
  public static void createUser(final int userId) {
    
    Cluster cluster = getCluster();
    Session session = cluster.connect(KEYSPACE);
    session.execute("INSERT INTO users "
        + "(userid) VALUES (" + userId + ");");
    cluster.close();
  }
  
  /**
   * Checks if an user has a task registered in the server. 
   * @param userId The ID of the user. 
   * @return True/False. 
   */
  public static boolean isUserRegistered(final int userId) {
    boolean found = false; 
    
    Cluster cluster = getCluster();
    Session session = cluster.connect(KEYSPACE);
    ResultSet r = session.execute("SELECT * FROM users WHERE userid=" 
        + userId);
    for (@SuppressWarnings("unused") Row row : r) {
      found = true;
    }
    cluster.close();
    return found;
  }
  
  /**
   * Returns the number of users stored in the database.
   * @return The number of users stored in the database.
   */
  public static int getNumberOfUsers() {
    
    Cluster cluster = getCluster();
    Session session = cluster.connect(KEYSPACE);
    ResultSet r = session.execute("SELECT * FROM users;");
    int count = 0;
    for (@SuppressWarnings("unused") Row row : r) {
      count++;
    }
    cluster.close();
    return count;
  }
  
  /**
   * Gets the Cluster. 
   * @return The cluster. 
   */
  private static Cluster getCluster() {
    return Cluster.builder().addContactPoint(DATABASE_IP)
        .withPort(DATABASE_PORT).build();
  }
}
