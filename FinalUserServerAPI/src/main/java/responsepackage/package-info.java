/**
 * This package contains the auxiliary classes to implement the custom responses
 * for the API. 
 * <br><br> 
 * AbstractStatusType: Necessary to construct the custom responses.
 * <br><br>
 * Responses (Each one implemented in one class): 
 * <ul>
 * <li> OutOfBounds: The number sent to the server is out of bounds.  
 * </ul>
 * 
 * @author David Garcia Centelles
 * 
 */
package responsepackage;