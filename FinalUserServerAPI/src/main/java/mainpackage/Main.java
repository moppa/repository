package mainpackage;

import com.wordnik.swagger.jaxrs.config.BeanConfig;

import persistencepackage.DB;

import java.io.IOException;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.ext.ContextResolver;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.moxy.json.MoxyJsonConfig;
import org.glassfish.jersey.server.ResourceConfig;

/**
 * Class that define the configuration and upload
 * of the server. 
 * 
 * @author David Garcia Centelles
 */
public final class Main {

    /**
     * URI of the Server. 
     */
    private static final URI BASE_URI
            = URI.create("http://172.31.18.169:8080/moppa/");

    /**
     * Void Constructor. 
     */
    private Main() {
      
    }
    
    /**
     * Configuration and upload of the server. 
     * @param args Arguments for main. 
     */
    public static void main(final String[] args) {
        try {
            BeanConfig beanConfig = new BeanConfig();
            beanConfig.setVersion("1.0");
            beanConfig.setScan(true);
            beanConfig.setResourcePackage(
                    TaskManager.class.getPackage().getName());
            beanConfig.setBasePath(BASE_URI.toString());
            beanConfig.setDescription("Task management for the user-side");
            beanConfig.setTitle("Task Manager User-Side");

            System.out.println("Initializing DB...");
            
            //We create the DB in case that doesn't exists. 
            DB.createDB();
            
            System.out.println("�DB Initialized!");
            
            final HttpServer server
                    = GrizzlyHttpServerFactory.createHttpServer(
                            BASE_URI, createApp());

            System.out.println(
                    String.format(
                            "Application started.%nHit enter to stop it..."));
            System.in.read();
            server.shutdownNow();
            System.exit(0);
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Create App. 
     * @return Resource Configuration. 
     */
    public static ResourceConfig createApp() {
        return new ResourceConfig().
                packages(TaskManager.class.getPackage().getName(),
                        "com.wordnik.swagger.jaxrs.listing").
                register(createMoxyJsonResolver()).
                register(new CrossDomainFilter());
    }

    /**
     * Create Moxy Json Resolver.
     * @return Context Resolver.  
     */
    public static ContextResolver<MoxyJsonConfig> createMoxyJsonResolver() {
        final MoxyJsonConfig moxyJsonConfig = new MoxyJsonConfig();
        Map<String, String> namespacePrefixMapper
                = new HashMap<String, String>(1);
        namespacePrefixMapper.put(
                "http://www.w3.org/2001/XMLSchema-instance", "xsi");
        moxyJsonConfig.setNamespacePrefixMapper(namespacePrefixMapper)
                .setNamespaceSeparator(':');
        return moxyJsonConfig.resolver();
    }

}