package mainpackage;

import java.io.StringReader;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;

//import persistencepackage.Cache;
import persistencepackage.DB;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;

import responsepackage.Invalid;
import responsepackage.NotRegistered;

/**
 * Class that define the functions of the API of final users:
 * <ul> 
 * <li> getUserId: The user requests an user Id. 
 * <li> SendTask: The user sends a task to the server.
 * <li> SeeTasks: The user sees the state and result of his tasks.
 * </ul> 
 * 
 * @author David Garcia Centelles
 * 
 */
@Path("task")
@Api(value = "/task", description = "Task Manager")
public final class TaskManager {
  
    /**
     * Counter that generates the ID of the Users. 
     */
    private static int countuser = DB.getNumberOfUsers() + 1;
    
    /**
     * Counter that generates the ID of the Tasks. 
     */
    private static int counttask = DB.getNumberOfTasks() + 1;
    
    /**
     * Response 200. 
     */
    static final int R200 = 200;
    
    /**
     * Response 400.
     */
    static final int R400 = 400;
    
    /**
     * Response 406.
     */
    static final int R406 = 406;
    
    /**
     * Max Number to calculate. 
     */
    static final int MAX_NUMBER = 100;     

  
    /**
     * Define the function of the API to send a notification to
     * the server to create an user Id. 
     * <br><br>
     * Parts:
     * <ul style="list-style-type:none"> 
     * <li> 1. Creates a response with the assigned ID of the user.
     * <li> 2. Add the User to the DB. 
     * <li> 3. Increment the User Counter.  
     * </ul>
     * @return The response with the assigned User ID.
     */
    @GET
    @Path("/getUserId")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Send a request to generate an user Id")
    @ApiResponses(value = {
        @ApiResponse(code = R200, message = "OK") })
    public Response getUserId() {
      
        JsonObject response = Json.createObjectBuilder()
           .add("user_id", countuser)
           .build();
        
        DB.createUser(countuser);
        countuser++;
        
        return Response.status(R200).entity(response).build();
    }   
    
    /**
     * Define the function of the API to send a task to the server.
     * <br><br>
     * Parts:
     * <ul style="list-style-type:none">
     * <li> 1. Interprets the request as is JSON.
     * <li> 2. Check if the number to calculate is inside the permitted 
     * boundaries.
     * <li> 2.1. (Positive case)
     * <li> 2.1.1. Adds the task to the database.
     * <li> 2.2. (Negative case)
     * <li> 2.2.1. Send and Error Code of "Out of Boundaries".
     * <li> 3. Return the response.
     * </ul>
     * @param request String in JSON format that represent the request
     * of the user.
     * @return The response to the user (200 OK, 400 Number Out of Bounds 
     * or 406 Unregistered User ID). 
     */
    @POST
    @Path("/sendTask")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Send a Task to the Server")
    @ApiResponses(value = {
        @ApiResponse(code = R200, message = "OK"),
        @ApiResponse(code = R400, message = "Number Out of Bounds"),
        @ApiResponse(code = R406, message = "Unregistered User ID") })
    public Response sendTask(final String request) {
      
        //Interpreting the received string as a JSON. 
        JsonReader jsonReader = Json.createReader(new StringReader(request));
        JsonObject obj = jsonReader.readObject();
        jsonReader.close();
        
        //Extracting the information of the request. 
        int number = obj.getInt("number_to_calculate");
        int userId = obj.getInt("user_id");
        
        if (!DB.isUserRegistered(userId)) {
          throw new NotRegistered("Unregistered User ID").except();
        }
        
        if ((number <= MAX_NUMBER) && (number > 0)) {
          
          //int result = Cache.findResultInCache(number);
          
          //If the task is inside the boundaries is added to the database. 
          DB.createTask(counttask, userId, number, "");
          counttask++;
          
          return Response.status(R200).build();
          
         } else {
           
           //If the task isn't inside the boundaries a response with a ID 0 is
           //created meaning that the number is out of boundaries. 
           throw new Invalid("Number Out of Bounds").except();
           
         }
    }
    
    /**
     * Define the function of the API to see the tasks of an user.
     * <br><br>
     * Parts:
     * <ul style="list-style-type:none"> 
     * <li> 1. Finds the tasks requested by the user. 
     * <li> 2. Creates the JSON Array to send with the response.
     * Contains the Number, the State and the Result of every task. 
     * </ul>
     * @param request String in JSON format that represent the request
     * of the user.  
     * @return The response to the user (200 OK + List of tasks or 
     * 406 Unregistered User ID). 
     */
    @POST
    @Path("/seeTasks")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "See the tasks requested by the user")
    @ApiResponses(value = {
        @ApiResponse(code = R200, message = "OK"),
        @ApiResponse(code = R406, message = "Unregistered User ID") })
    public Response seeTasks(final String request) {
      
        //Interpreting the received string as a JSON. 
        JsonReader jsonReader = Json.createReader(new StringReader(request));
        JsonObject obj = jsonReader.readObject();
        jsonReader.close();
        
        //Extracting the information of the request. 
        int userId = obj.getInt("user_id");
 
        if (!DB.isUserRegistered(userId)) {
          throw new NotRegistered("Unregistered User ID").except();
        } else {
          //Building the JSON Array with the tasks of the user. 
          JsonArray tasks = DB.getTasksOfUser(userId);
          
          return Response.status(R200).entity(tasks).build();
        }
    } 
    
    /**
     * Define the function of the API to erase the history of tasks of an user.
     * <br><br>
     * @param request String in JSON format that represent the request
     * of the user.
     * @return The response to the user (200 OK or 
     * 406 Unregistered User ID). 
     */
    @POST
    @Path("/eraseHistory")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Erases the history of tasks of an user")
    @ApiResponses(value = {
        @ApiResponse(code = R200, message = "OK"),
        @ApiResponse(code = R406, message = "Unregistered User ID") })
    public Response eraseHistory(final String request) {
      
        //Interpreting the received string as a JSON. 
        JsonReader jsonReader = Json.createReader(new StringReader(request));
        JsonObject obj = jsonReader.readObject();
        jsonReader.close();
        
        //Extracting the information of the request. 
        int userId = obj.getInt("user_id");
 
        if (!DB.isUserRegistered(userId)) {
          throw new NotRegistered("Unregistered User ID").except();
        } else {
          //Building the JSON Array with the tasks of the user. 
          
          DB.eraseTasksfromUser(userId);
          
          return Response.status(R200).build();
        }
    }  
}