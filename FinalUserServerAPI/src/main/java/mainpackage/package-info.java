/**
 * This package contains the main classes necessary to configure 
 * and upload the server and define the different functions of the API.
 * <br><br> 
 * Main: Configuration and Upload of Server.
 * <br><br>
 * TaskManager: Define the functions of the API of final users:
 * <ul>
 * <li> getUserId: The user requests an user Id. 
 * <li> SendTask: The user sends a task to the server.   
 * <li> SeeTasks: The user sees the state and result of his tasks.
 * </ul>
 * 
 * @author David Garcia Centelles
 */
package mainpackage;