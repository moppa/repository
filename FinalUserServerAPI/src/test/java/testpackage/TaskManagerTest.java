package testpackage;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Session;

import mainpackage.TaskManager;


/**
 * The class <code>TaskManagerTest</code> contains tests for the class 
 * <code>{@link TaskManager}</code>.
 *
 * @author David Garcia Centelles
 * @author Irving David Abundiz Torres
 */
public class TaskManagerTest {

    /**
     * IP of the Database. 
     */
    private static final String DATABASE_IP = "52.34.80.38";
    
    /**
     * Port of the Database. 
     */
    private static final int DATABASE_PORT = 32771;
      
    /**
     * Keyspace used in the database. 
     */
    private static final String KEYSPACE = "moppa";
  
	/**
	 * Run the TaskManager() constructor test.
	 * 
	 * @throws Exception Exception
	 */
	@Test
	public final void testTaskManager1()
		throws Exception {
	  
		TaskManager result = new TaskManager();
		assertNotNull(result);
	}

	/**
	 * Run the Response getUserId() method test.
	 *
     * @throws Exception Exception
	 */
	@Test
	public final void getUserId1()
		throws Exception {
		TaskManager fixture = new TaskManager();
		
		Response result = fixture.getUserId();
		assertEquals(Response.Status.OK.getStatusCode(), result.getStatus());
		
		JsonObject entity = (JsonObject) result.getEntity();
		assertEquals("{\"user_id\":4}", entity.toString());
	}	
	
	/**
	 * Run the Response seeTasks(String) method test.
	 *
     * @throws Exception Exception
	 */
	@Test
	public final void testSeeTasks1()
		throws Exception {
		TaskManager fixture = new TaskManager();
		
		//With a registered ID in the DB. 
		String request = "{\"user_id\": 2}";
		
		Response result = fixture.seeTasks(request);
		assertEquals(Response.Status.OK.getStatusCode(), result.getStatus());
		
		JsonArray entity = (JsonArray) result.getEntity();
		assertEquals("[{\"Number\":2,\"Status\":\"TASK FINISHED\","
		    + "\"Result\":\"2\"}]", entity.toString());
	}

	/**
	 * Run the Response seeTasks(String) method test.
	 *
     * @throws Exception Exception
	 */
	@Test
	public final void testSeeTasks2()
		throws Exception {
		TaskManager fixture = new TaskManager();
		
		//With a not registered ID in the DB. 
		String request = "{\"user_id\": 0}";

		try {
			fixture.seeTasks(request);
		} catch (WebApplicationException e) {
			assertEquals(Response.Status.NOT_FOUND.getStatusCode(), 
			    e.getResponse().getStatus());
		}
	}

	/**
	 * Run the Response sendTask(String) method test.
	 *
     * @throws Exception Exception
	 */
	@Test
	public final void testSendTask1()
		throws Exception {
		TaskManager fixture = new TaskManager();
		
		//With registered User ID and Valid number (1 - 100). 
		String request = "{\"user_id\": 3, \"number_to_calculate\": 42}";

		Response result = fixture.sendTask(request);
		assertEquals(Response.Status.OK.getStatusCode(), result.getStatus());
	}

	/**
	 * Run the Response sendTask(String) method test.
	 *
     * @throws Exception Exception
	 */
	@Test
	public final void testSendTask2()
		throws Exception {
		TaskManager fixture = new TaskManager();
		
		//With registered User ID and invalid number (< 0). 
		String request = "{\"user_id\": 3, \"number_to_calculate\": -42}";

		try {
			fixture.sendTask(request);
		} catch (WebApplicationException e) {
			assertEquals(Response.Status.NOT_ACCEPTABLE.getStatusCode(), 
			    e.getResponse().getStatus());
		}
	}

	/**
	 * Run the Response sendTask(String) method test.
	 *
     * @throws Exception Exception
	 */
	@Test
	public final void testSendTask3()
		throws Exception {
		TaskManager fixture = new TaskManager();
		
		//With registered User ID and invalid number (> 100). 
		String request = "{\"user_id\": 3, \"number_to_calculate\": 142}";

		try {
			fixture.sendTask(request);
		} catch (WebApplicationException e) {
			assertEquals(Response.Status.NOT_ACCEPTABLE.getStatusCode(), 
			    e.getResponse().getStatus());
		}
	}

	/**
	 * Run the Response sendTask(String) method test.
	 *
     * @throws Exception Exception
	 */
	@Test
	public final void testSendTask4()
		throws Exception {
		TaskManager fixture = new TaskManager();
		
		//With not registered User ID and Valid number (1 - 100). 
		String request = "{\"user_id\": 0, \"number_to_calculate\": 42}";

		try {
			fixture.sendTask(request);
		} catch (WebApplicationException e) {
			assertEquals(Response.Status.NOT_FOUND.getStatusCode(), 
			    e.getResponse().getStatus());
		}
	}

	/**
	 * Run the Response sendTask(String) method test.
	 *
     * @throws Exception Exception
	 */
	@Test
	public final void testSendTask5()
		throws Exception {
		TaskManager fixture = new TaskManager();
		
		//With not registered User ID and invalid number (< 0). 
		String request = "{\"user_id\": 0, \"number_to_calculate\": -42}";

		try {
			fixture.sendTask(request);
		} catch (WebApplicationException e) {
			assertEquals(Response.Status.NOT_FOUND.getStatusCode(), 
			    e.getResponse().getStatus());
		}
	}
	
	/**
	 * Run the Response sendTask(String) method test.
	 *
     * @throws Exception Exception
	 */
	@Test
	public final void testSendTask6()
		throws Exception {
		TaskManager fixture = new TaskManager();
		
		//With not registered User ID and invalid number (> 100). 
		String request = "{\"user_id\": 0, \"number_to_calculate\": 142}";

		try {
			fixture.sendTask(request);
		} catch (WebApplicationException e) {
			assertEquals(Response.Status.NOT_FOUND.getStatusCode(), 
			    e.getResponse().getStatus());
		}
	}
	
	/**
	 * Run the Response eraseHistory(String) method test.
	 *
     * @throws Exception Exception
	 */
	@Test
	public final void testEraseHistory1()
		throws Exception {
		TaskManager fixture = new TaskManager();
		
		//With a registered ID in the DB. 
		String request = "{\"user_id\": 1}";
		
		Response result = fixture.eraseHistory(request);
		assertEquals(Response.Status.OK.getStatusCode(), result.getStatus());
	}

	/**
	 * Run the Response eraseHistory(String) method test.
	 *
     * @throws Exception Exception
	 */
	@Test
	public final void testEraseHistory2()
		throws Exception {
		TaskManager fixture = new TaskManager();
		
		//With a not registered ID in the DB. 
		String request = "{\"user_id\": 0}";

		try {
			fixture.eraseHistory(request);
		} catch (WebApplicationException e) {
			assertEquals(Response.Status.NOT_FOUND.getStatusCode(), 
			    e.getResponse().getStatus());
		}
	}
	
	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 */
	@BeforeClass
    public static final void setUp()
		throws Exception {

	    addTestValuestoTables();
        
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 */
	@AfterClass
    public static final void tearDown()
		throws Exception {
	     
        resetValues();
        
	}
	
	   /**
     * Initializes the test values. 
     */
    private static void addTestValuestoTables() {
         
        Cluster cluster = getCluster();    
         Session session = cluster.connect(KEYSPACE);
         
         String insertasks1 = "INSERT INTO tasks"
             + "(taskid, numbertocalculate, userid, result, state) "
             + "VALUES (1,2,1,'2',2);";
         String insertasks2 = "INSERT INTO tasks"
             + "(taskid, numbertocalculate, userid, result, state) "
             + "VALUES (2,4,1,'',1);";
         String insertasks3 = "INSERT INTO tasks"
             + "(taskid, numbertocalculate, userid, result, state) "
             + "VALUES (3,6,1,'',0);";
         String insertasks4 = "INSERT INTO tasks"
             + "(taskid, numbertocalculate, userid, result, state) "
             + "VALUES (4,2,2,'2',2);";
         
         String insertmobiles1 = "INSERT INTO mobiles "
             + "(mobileid, assignedtaskid) VALUES (1,0);";
         String insertmobiles2 = "INSERT INTO mobiles "
             + "(mobileid, assignedtaskid) VALUES (2,0);";
         String insertmobiles3 = "INSERT INTO mobiles "
             + "(mobileid, assignedtaskid) VALUES (3,2);";
         
         String insertusers1 = "INSERT INTO users (userId) VALUES (1);";
         String insertusers2 = "INSERT INTO users (userId) VALUES (2);";
         String insertusers3 = "INSERT INTO users (userId) VALUES (3);";
         
         session.execute(insertasks1);
         session.execute(insertasks2);
         session.execute(insertasks3);
         session.execute(insertasks4);
         
         session.execute(insertmobiles1);
         session.execute(insertmobiles2);
         session.execute(insertmobiles3);
         
         session.execute(insertusers1);
         session.execute(insertusers2);
         session.execute(insertusers3);
         
         cluster.close();
     }
    
    /**
     * Reset the test values. 
     */
    private static void resetValues() {
        
        Cluster cluster = getCluster();     
        Session session = cluster.connect(KEYSPACE);
        
        String table1 = "CREATE TABLE tasks (taskId int,numberToCalculate int,"
            + "userId int,result varchar,state int,"
            + "PRIMARY KEY (taskId,userId));";
        String table2 = "CREATE TABLE mobiles (mobileId int,assignedTaskId int,"
            + "PRIMARY KEY (mobileId));";
        String table3 = "CREATE TABLE users "
            + "(userId int,PRIMARY KEY (userId));";     
       
        String droptable1 = "DROP table tasks;";
        String droptable2 = "DROP table mobiles;";
        String droptable3 = "DROP table users;";
        
        session.execute(droptable1);
        session.execute(droptable2);
        session.execute(droptable3);
        session.execute(table1);
        session.execute(table2);
        session.execute(table3);
        
        cluster.close();
    }
    
	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 */
	public static void main(final String[] args) {
		new org.junit.runner.JUnitCore().run(TaskManagerTest.class);
	}
		  
	/**
	 * Gets the Cluster. 
	 * @return The cluster. 
	 */
	private static Cluster getCluster() {
	   return Cluster.builder().addContactPoint(DATABASE_IP)
	      .withPort(DATABASE_PORT).build();
	}	
}